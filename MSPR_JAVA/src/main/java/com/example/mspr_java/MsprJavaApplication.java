package com.example.mspr_java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsprJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsprJavaApplication.class, args);
	}

}
