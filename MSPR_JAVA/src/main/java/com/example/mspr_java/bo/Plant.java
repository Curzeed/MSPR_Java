package com.example.mspr_java.bo;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name = "PLANT")
public class Plant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "LABEL")
    private String label;

    @Column(name = "DETAILS")
    private String details;

    @OneToMany(mappedBy = "plants")
    Set<Post> posts;


    public Plant() {
    }

    public Plant(int id, String label, String details) {
        this.id = id;
        this.label = label;
        this.details = details;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
