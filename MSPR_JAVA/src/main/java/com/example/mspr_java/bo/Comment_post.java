package com.example.mspr_java.bo;

import jakarta.persistence.*;

@Entity
@Table(name = "COMMENT_POST")
public class Comment_post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "CONTENT")
    private String content;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    User user;

    @ManyToOne
    @JoinColumn(name = "POST_ID")
    Post post;

    public Comment_post() {
    }

    public Comment_post(Integer id, String content) {
        this.id = id;
        this.content = content;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @Override
    public String toString() {
        return "Comment_post{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", user2=" + user +
                ", post=" + post +
                '}';
    }
}
