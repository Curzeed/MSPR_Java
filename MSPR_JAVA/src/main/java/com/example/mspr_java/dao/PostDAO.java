package com.example.mspr_java.dao;

import com.example.mspr_java.bo.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("http://localhost:4200/")
public interface PostDAO extends CrudRepository<Post, Integer>{



}
