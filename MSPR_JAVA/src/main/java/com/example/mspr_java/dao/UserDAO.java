package com.example.mspr_java.dao;

import com.example.mspr_java.bo.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;


@CrossOrigin("http://localhost:4200/")
public interface UserDAO extends CrudRepository<User, Integer> {

        @RestResource(path = "by-username")
         User findByUsername(String username);
}



