package com.example.mspr_java.dao;

import com.example.mspr_java.bo.Plant;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.CrossOrigin;


@CrossOrigin("http://localhost:4200/")
public interface PlantDAO extends CrudRepository<Plant, Integer> {
}
